# README #

#  Technical Test - Deployment #

## Problem 1 ##
This is a RESTful API which provides a service for storing, updating, retrieving and deleting Person entities.
The Person entity has a `first_name`, `last_name`, `age`, and `favourite_colour`.

## Build ##

Make sure you have installed the latest JDK 8 and Apache Maven 3.6.

Execute the following command to build a clean package locally.

```bash
mvn clean package
```

## Deployment ##
Currenlty the app is configured to run on TomEE as well as can be packaged to run in a docker container

After executing the command below, the application will be deployed on a TomEE server and the same will start 

```bash
mvn package tomee:run
```

### Docker ##
Build the docker image.

```bash
$docker build --pull --rm -f "Dockerfile.wildfly" -t snhane/technical_test_embl:latest "."

[+] Building 13.5s (7/7) FINISHED                                                                     
 => [internal] load build definition from Dockerfile.wildfly                                                                           0.0s
 ...
```

### Running applications from Docker Images ###

Check all docker images we've  created.

```bash
$technical_test_embl llockkuzs$ docker image ls | grep snhane
snhane/technical_test_embl   latest    4782a6810f45   9 minutes ago    726MB
```

Now you can run your application from Docker images directly.

```bash
$docker run --rm -it  -p 8080:8080/tcp snhane/technical_test_embl:latest
```

Verify if it is working as expected.

```bash
$curl http://localhost:8080/technical_test_embl/api/
{}
```

## API Operations available ##

REST Application: http://localhost:8080/technical_test_embl/api             -> com.gmail.at.nsteliosergio.resources.JaxrsActivator@5e62ac08
     Service URI: http://localhost:8080/technical_test_embl/api/            -> Pojo com.gmail.at.nsteliosergio.resources.PersonResource

           DELETE http://localhost:8080/technical_test_embl/api/{id}/delete ->      Response delete(Long)              
              GET http://localhost:8080/technical_test_embl/api/            ->      Response getAllPerson()            
              GET http://localhost:8080/technical_test_embl/api/list        ->      Response listPerson()              
              GET http://localhost:8080/technical_test_embl/api/{id}        ->      Response getPerson(Long)           
             POST http://localhost:8080/technical_test_embl/api/            ->      Response create(String)      
             POST http://localhost:8080/technical_test_embl/api/store       ->      Response store(String)       
              PUT http://localhost:8080/technical_test_embl/api/{id}/update ->      Response update(Long, PersonEntity)

