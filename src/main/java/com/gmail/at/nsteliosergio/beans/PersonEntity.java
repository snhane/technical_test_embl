package com.gmail.at.nsteliosergio.beans;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonRootName(value = "person")
@Entity
@Table(name = "person")
@NamedQueries({ 
    @NamedQuery(name = "Person.findAll", query = "SELECT p FROM PersonEntity p"), 
    @NamedQuery(name = "Person.findByLastAndFirstName", query = "SELECT p FROM PersonEntity p WHERE p.firstName=:firstName AND p.lastName=:lastName") })
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PersonEntity implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private String favouriteColour;

    private Long age;

    private String updatedTime;

    public PersonEntity(){
        super();
    }

    public PersonEntity(String firstName, String lastName, String favouriteColour){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.favouriteColour = favouriteColour;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param firstName
     */
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @param lastName
     */
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @param favouriteColour
     */
    public String getFavouriteColour() {
        return this.favouriteColour;
    }

    public void setFavouriteColour(final String favouriteColour) {
        this.favouriteColour = favouriteColour;
    }

    /**
     * @param age the age to set
     */
    public Long getAge() {
        return this.age;
    }

    public void setAge(final Long age) {
        this.age = age;
    }

    @PreUpdate
    private void onUpdated() {
        this.updatedTime = ZonedDateTime.now().toLocalDateTime().toString();
    }

    public String getUpdatedTime() {
        return this.updatedTime;
    }

    @Override
    public String toString() {
        return "person{" + "id=" + id + ", firstName=" + this.getFirstName() + ", lastName=" + this.getLastName()
                + ", age=" + this.getAge() + ", favouriteColour=" + this.getFavouriteColour() + '}';
    }

}