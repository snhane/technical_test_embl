package com.gmail.at.nsteliosergio.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * The application path is the root
 */
@ApplicationPath("/api")
public class JaxrsActivator extends Application {
}
