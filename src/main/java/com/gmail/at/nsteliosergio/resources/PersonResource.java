package com.gmail.at.nsteliosergio.resources;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gmail.at.nsteliosergio.beans.PersonEntity;
import com.gmail.at.nsteliosergio.services.PersonService;

@Path("")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonResource {

    @Inject
    PersonService personService;

    @POST
    @Path("store")
    public Response store(String personString) {
        // System.out.println("### " + personString + "###");
        PersonEntity person = personService.deserialize(personString);
        // System.out.println(person);
        personService.create(person);
        
        return this.composeResponse(Response.ok().build());
    }

    @POST
    public Response create(String personString) {
        PersonEntity person = personService.deserialize(personString);
        personService.create(person);
        return this.composeResponse(Response.ok().build());
    }

    @GET
    @Path("{id}")
    public Response getPerson(@PathParam("id") Long id) {
        return this.composeResponse(Response.ok(personService.findById(id)).build());
    }

    @GET
    @Path("")
    public Response getAllPerson() {
        return this.composeResponse(Response.ok(personService.getAll()).build());
    }

    @GET
    @Path("list")
    public Response listPeople() {
        return this.composeResponse(Response.ok(personService.getAll()).build());
    }

    @PUT
    @Path("{id}/update")
    public Response update(@PathParam("id") Long id, PersonEntity person) {
        PersonEntity updatePerson = personService.findPersonById(id);

        updatePerson.setAge(person.getAge());
        updatePerson.setFavouriteColour(person.getFavouriteColour());
        personService.update(updatePerson);

        return this.composeResponse(Response.ok().build());
    }

    @DELETE
    @Path("{id}/delete")
    public Response delete(@PathParam("id") Long id) {
        PersonEntity person = personService.findPersonById(id);
        personService.delete(person);

        return this.composeResponse(Response.ok().build());
    }

    /**
     * Adds the custom header to any resquest
     * 
     * @param response
     * @return
     */
    public Response composeResponse(Response response) {
        System.out.println(response.getHeaders());
        return response;
    }

}