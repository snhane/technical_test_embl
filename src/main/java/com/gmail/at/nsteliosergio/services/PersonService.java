package com.gmail.at.nsteliosergio.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gmail.at.nsteliosergio.beans.PersonEntity;

@Stateless
public class PersonService {

    @PersistenceContext(unitName = "technical_test_embl_PU")
    EntityManager entityManager;

    public void create(PersonEntity person) {
        entityManager.persist(person);
    }

    public PersonEntity findPersonById(Long id) {
        return entityManager.find(PersonEntity.class, id);
    }

    public String findById(Long id) {
        return this.serialize(entityManager.find(PersonEntity.class, id));
    }

    public String findByLastAndFirstName(String lastName, String firstName) {
        return this.serialize(entityManager.createNamedQuery("Person.findByLastAndFirstName", PersonEntity.class)
                .setParameter("lastName", lastName).setParameter("firstName", firstName).getSingleResult());
    }

    /*
     * public List<PersonEntity> getPeople() { return
     * entityManager.createNamedQuery("Person.findAll",
     * PersonEntity.class).getResultList(); }
     */

    public String getAll() {
        return this.serialize(entityManager.createNamedQuery("Person.findAll", PersonEntity.class).getResultList());
    }

    public List<PersonEntity> getPeople() {
        CriteriaQuery<PersonEntity> cq = entityManager.getCriteriaBuilder().createQuery(PersonEntity.class);
        cq.select(cq.from(PersonEntity.class));
        return entityManager.createQuery(cq).getResultList();
    }

    public void update(PersonEntity person) {
        entityManager.merge(person);
    }

    public void delete(PersonEntity person) {
        if (!entityManager.contains(person)) {
            person = entityManager.merge(person);
        }

        entityManager.remove(person);
    }

    public void clear() {
        Query removeAll = entityManager.createQuery("delete from Person");
        removeAll.executeUpdate();
    }

    public String serialize(PersonEntity person) {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.WRAP_ROOT_VALUE);

        try {
            return om.writeValueAsString(person);

        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    public PersonEntity deserialize(String personString){
        ObjectMapper om = new ObjectMapper();
        PersonEntity person = null;
        try {
            person = om.readValue(personString, PersonEntity.class);
            
        } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
        }
        return person;

    }

    private String serialize(List<PersonEntity> person) {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED);
        // om.enable(SerializationFeature.WRAP_ROOT_VALUE);

        try {
            return om.writeValueAsString(person);

        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }
}