package com.gmail.at.nsteliosergio.services;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
// import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gmail.at.nsteliosergio.beans.PersonEntity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class PersonServiceTest {

    @InjectMocks
    PersonService personService;

    @Mock
    private PersonEntity thePerson;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        thePerson = new PersonEntity();
        thePerson.setLastName("Casava");
        thePerson.setFirstName("John");
        thePerson.setAge(25L);
        thePerson.setFavouriteColour("Green");
    }

    @After
    public void tearDown() {
        thePerson = null;
    }

    @Test
    @Ignore
    public void shouldCreatePerson() {
        // verify(thePerson);
        personService.create(thePerson);
    }

    @Test
    @Ignore
    public void shouldFindPerson(){
        final Long id = 1L;
        String jsonString = personService.findById(id);
        System.out.println(jsonString);
    }

    @Test
    public void testSerializing() throws JsonProcessingException {

        final String jsonString = personService.serialize(thePerson);

        System.out.println(jsonString);
        assertThat(jsonString, containsString("John"));
        assertThat(jsonString, containsString("last_name"));

    }

    @Test
    public void testDeserializing() throws JsonProcessingException {
        String jsonString = "{\"first_name\": \"John\", \"last_name\": \"Casava\", \"age\": \"25\", \"favourite_colour\": \"Green\"}";

        PersonEntity thePerson = personService.deserialize(jsonString);
        assertNotNull("Deserialized string is not null", thePerson);
        assertThat(thePerson.getFirstName(),containsString("John"));
    }

    @Test
    public void testRootName() throws JsonProcessingException {

        final String jsonString = personService.serialize(thePerson);

        System.out.println(jsonString);
        assertThat("String contains the root: ", jsonString, containsString("person"));
    }

}